require 'app/modules.rb'

def tick args
  args.state.game_initialized ||= false
  unless args.state.game_initialized
    $game = Game.new(args)
    args.state.game_initialized = true
  end
  $game.args = args
  $game.tick
end

$gtk.reset
$game = nil
