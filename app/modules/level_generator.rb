class LevelGenerator
  include GameEntities
  attr_gtk

  def initialize(args)
    @args = args
  end

  def generate
    state.current_scene = :level
    state.warp = :none
    create_entities(state) do |e|
      if state.player_ship.nil?
        if state.player_data&.health_remaining
          e.player_ship(x: 500, y: 200, health: state.player_data.health_remaining)
        else
          e.player_ship(x: 500, y: 200)
        end
      else
        state.player_ship.x = 500
        state.player_ship.y = 200
      end

      [
        Area.new(40, 40, 520, 600),
        Area.new(641, 40, 520, 600)
      ].each do |area|
        interval = state.level_count > 250 ? 3000 : 8000 - state.level_count * 20
        e.planet(x: area.random_x, y: area.random_y,
          generation_interval: interval,
          generation_timer: interval - 500
        )
      end
    end

    assign_warp
  end

  def clear_level
    delete_entity(state, state.player_ship)
    delete_entity(state, state.warp)
    delete_all(state, state.enemies)
    delete_all(state, state.planets)
    delete_all(state, state.bullets)
    delete_all(state, state.enemy_bullets)
    state.enemies = []
    state.planets = []
    state.bullets = []
    state.enemy_bullets = []
  end

  def generate_intermission
    state.current_scene = :intermission
    state.intermission_timer = 0
    state.intermission_end = 5000
    create_entities(state) do |b|
      b.label(x: grid.w / 2 - 80, y: grid.h / 2 - 10, text: "Level Complete!", id: :level_complete)
    end
  end

  def clear_intermission
    delete_entity(state, state.labels[:level_complete])
  end

  def generate_title
    state.current_scene = :title
    state.ready_to_play = :no
    create_entities(state) do |b|
      b.label(x: grid.w / 2 - 80, y: grid.h / 2 - 10, text: "Astrolet!", id: :title)
      b.label(x: grid.w / 2 - 140, y: grid.h / 2 - 40,
        text: "press enter to begin", id: :press_start)
    end
  end

  def clear_title
    delete_entity(state, state.labels[:title])
    delete_entity(state, state.labels[:press_start])
  end

  def generate_death
    state.current_scene = :death
    state.continue = :no
    create_entities(state) do |b|
      b.label(x: grid.w / 2 - 80, y: grid.h / 2 - 10, text: "Game Over", id: :message)
      b.label(x: grid.w / 2 - 200, y: grid.h / 2 - 40,
        text: "press enter to return to title screen", id: :press_continue)
    end
  end

  def clear_death
    delete_entity(state, state.labels[:message])
    delete_entity(state, state.labels[:press_continue])
  end

  private

  class Area
    def initialize(x, y, w, h)
      @x = x
      @y = y
      @w = w
      @h = h
    end

    def random_x
      @x + Random.rand(@w)
    end

    def random_y
      @y + Random.rand(@h)
    end
  end

  def assign_warp
    state.planets[Random.rand(state.planets.count)].contains_warp = true
  end
end
