class SceneSystem < GameSystem
  include GameEntities

  def init(args)
    args.state.level_count = 0
  end

  def run
    case state.current_scene
    when :level
      if player_is_dead
        lg = LevelGenerator.new(args)
        lg.clear_level
        lg.generate_death
      elsif level_is_won
        add_level_points
        store_player_data
        increment_level
        lg = LevelGenerator.new(args)
        lg.clear_level
        lg.generate_intermission
      end
    when :intermission
      state.intermission_timer += DELTA_TIME
      if intermission_ended
        lg = LevelGenerator.new(args)
        lg.clear_intermission
        lg.generate
      end
    when :title
      if state.ready_to_play == :ready
        state.score = 0
        lg = LevelGenerator.new(args)
        lg.clear_title
        lg.generate
        state.player_ship.health = 10
      end
    when :death
      if state.continue == :yes
        lg = LevelGenerator.new(args)
        lg.clear_death
        lg.generate_title
      end
    end
  end

  private

  def add_level_points
    state.score += 500
  end

  def increment_level
    state.level_count += 1
  end

  def intermission_ended
    state.intermission_timer >= state.intermission_end
  end

  def store_player_data
    state.player_data.health_remaining = 0
    state.player_data.health_remaining = state.player_ship.health
  end

  def level_is_won
    state.warp != :none && state.warp.w.is_a?(Numeric) && collision(state.player_ship, state.warp)
  end

  def player_is_dead
    state.player_ship.health <= 0
  end

  def collision(a, b)
    a.intersect_rect? b
  end
end
