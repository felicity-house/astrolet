class BoundaryWarpSystem < GameSystem
  def run
    warpables = []
    warpables << state.player_ship unless state.player_ship.nil?
    warpables += state.enemies
    warpables += state.bullets
    warpables += state.enemy_bullets
    warpables.each { |warpable| warp(warpable) }
  end

  def warp(warpable)
    x = warpable.x
    y = warpable.y

    if x < -40
      x = 1280
    elsif x > 1300
      x = 0
    end

    if y < -40
      y = 720
    elsif y > 740
      y = 0
    end

    warpable.x = x
    warpable.y = y
  end
end
