class DamageSystem < GameSystem
  include GameEntities

  def run
    return false if state.player_ship.nil?
    state.bullets.each do |bullet|
      state.planets.each do |planet|
        if collision(bullet, planet)
          planet.health -= 1
          delete_entity(state, bullet)
          if planet.health <= 0
            Audio.play(args, :planet_explosion)
            delete_entity(state, planet)
            state.score += 200
            if planet.contains_warp
              create_entities(state) do |b|
                b.warp(x: planet.x, y: planet.y)
              end
            end
          end
        end
      end

      state.enemies.each do |enemy|
        if collision(bullet, enemy)
          Audio.play(args, :enemy_ship_damage)
          enemy.health -= 1
          delete_entity(state, bullet)
          if enemy.health <= 0
            Audio.play(args, :enemy_death)
            delete_entity(state, enemy)
            if state.score_meta.current_from_enemies < state.score_meta.max_from_enemies
              remainder_score_from_enemies = state.score_meta.max_from_enemies - state.score_meta.current_from_enemies
              if remainder_score_from_enemies > 10
                state.score += 10
              else
                state.score += remainder_score_from_enemies
              end
            end
          end
        end
      end
    end

    state.enemy_bullets.each do |bullet|
      if collision(bullet, state.player_ship)
        Audio.play(args, :ship_damage)
        state.player_ship.health -= 1
        delete_entity(state, bullet)
        if state.player_ship.health <= 0
          state.player_ship.is_dead = true
          Audio.play(args, :player_death)
        end
      end
    end
  end

  def collision(a, b)
    a.intersect_rect? b
  end
end
