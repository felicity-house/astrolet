class PhysicsSystem < GameSystem
  def run
    return if state.player_ship.nil?
    move_movable(state.player_ship)
    state.enemies.each {|e| move_movable(e)}
    move_bullets
  end

  def move_movable(movable)
    movable.angle += movable.vel_angle

    case movable.state
    when :idle
      vel_vect = Vect.new(movable.vel_x, movable.vel_y)
      movable.vel_x = 0 if movable.vel_x.abs < 0.05
      movable.x += movable.vel_x
      movable.vel_y = 0 if movable.vel_y.abs < 0.05
      movable.y += movable.vel_y
    when :thrust
      acc_vect = Vect.from_degrees(movable.thrust_acc, movable.angle)
      vel_vect = Vect.new(movable.vel_x, movable.vel_y)
      movable.vel_x += acc_vect.x
      movable.vel_x = 0 if acc_vect.x.zero? && movable.vel_x.abs < 0.05
      movable.x += movable.vel_x
      movable.vel_y += acc_vect.y
      movable.vel_y = 0 if acc_vect.y.zero? && movable.vel_y.abs < 0.05
      movable.y += movable.vel_y
    when :brake
      vel_vect = Vect.new(movable.vel_x, movable.vel_y)
      return if vel_vect.magnitude.zero?
      acc_vect = Vect.from_degrees(movable.brake_acc, vel_vect.angle)

      a_x_abs = acc_vect.x.abs
      v_x_abs = movable.vel_x.abs
      if a_x_abs > v_x_abs
        movable.vel_x = 0
      else
        movable.vel_x += acc_vect.x
      end

      a_y_abs = acc_vect.y.abs
      v_y_abs = movable.vel_y.abs
      if a_y_abs > v_y_abs
        movable.vel_y = 0
      else
        movable.vel_y += acc_vect.y
      end

      movable.x += movable.vel_x
      movable.y += movable.vel_y
    end
  end

  def move_bullets
    state.bullets.each do |bullet|
      bullet.x += bullet.vel_x
      bullet.y += bullet.vel_y
    end

    state.enemy_bullets.each do |bullet|
      bullet.x += bullet.vel_x
      bullet.y += bullet.vel_y
    end
  end
end
