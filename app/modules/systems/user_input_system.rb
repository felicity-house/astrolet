class UserInputSystem < GameSystem
  include GameEntities

  def run
    if keyboard.key_down.hyphen
      Audio.dec_volume(args)
    end

    if keyboard.key_down.equal_sign
      Audio.inc_volume(args)
    end

    case state.current_scene
    when :level
      return if state.player_ship.nil?

      state.player_ship.state = :idle

      if keyboard.up
        state.player_ship.state = :thrust
      end

      if keyboard.down
        state.player_ship.state = :brake
      end

      state.player_ship.vel_angle = 0.0

      if keyboard.left
        state.player_ship.vel_angle = 3.5
      end

      if keyboard.right
        state.player_ship.vel_angle = -3.5
      end

      if keyboard.key_down.space
        create_entities(state) do |b|
          vect = Vect.new(state.player_ship.x + 17, state.player_ship.y + 17) +
                 Vect.from_degrees(20, state.player_ship.angle)
          b.bullet(
            x: vect.x,
            y: vect.y,
            vel_x: state.player_ship.vel_x,
            vel_y: state.player_ship.vel_y,
            angle: state.player_ship.angle
          )
        end
        Audio.play(args, :bullet)
      end
    when :title
      if keyboard.key_down.enter
        state.ready_to_play = :ready
      end
    when :death
      if keyboard.key_down.enter
        state.continue = :yes
      end
    end
  end
end
