class BulletSystem < GameSystem
  def run
    state.bullets.each do |bullet|
      bullet.time_remaining -= DELTA_TIME
    end

    state.game_entities.keep_if { |e| e.entity_type != :bullet || e.time_remaining > 0}
    state.bullets.keep_if { |b| b.time_remaining > 0 }

    state.enemy_bullets.each do |bullet|
      bullet.time_remaining -= DELTA_TIME
    end

    state.game_entities.keep_if { |e| e.entity_type != :enemy_bullet || e.time_remaining > 0}
    state.enemy_bullets.keep_if { |b| b.time_remaining > 0 }
  end
end
