class UiSystem < GameSystem
  include GameEntities

  def init(args)
    args.state.healthbar_count ||= 0
    create_entities(args.state) do |b|
      10.times do |i|
        b.health_bar_item(x: 20 + 15 * i, y: args.grid.h - 30)
      end

      b.label(id: :score, x: args.grid.w / 2 - 80, y: args.grid.h - 30, text: "Score: 0")
    end
  end

  def run
    return if state.player_ship.nil?
    health
    score
  end

  def health
    health = state.player_ship.health
    if health != state.healthbar_count
      state.healthbar.each_with_index do |h, i|
        if i < health
          h.healthy = true
        else
          h.healthy = false
        end
      end
      state.healthbar_count = health
    end
  end

  def score
    state.labels.score.text = "Score: #{state.score}"
  end
end
