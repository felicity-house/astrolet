class AiSystem < GameSystem
  include GameEntities

  ENEMY_RANGE = 200

  def run
    return false if state.player_ship.nil?
    state.enemies.each { |e| update_timers(e) }

    if state.tick_count % 1 == 0
      state.enemies.each { |e| enemy_think(e) }
    end
  end

  private

  def enemy_think(enemy)
    decide_on_movement(enemy)
    decide_on_fire_action(enemy)
    act_movement(enemy)
    act_fire(enemy)
  end

  def decide_on_fire_action(enemy)
    if in_firing_range_of_player(enemy)
      enemy.fire_action = :fire
    else
      enemy.fire_action = :hold
    end
  end

  def act_fire(enemy)
    case enemy.fire_action
    when :fire
      fire_if_ready(enemy)
    end
  end

  def decide_on_movement(enemy)
    enemy.action = :approach
    # if within_range?(enemy)
    #   if facing_player?(enemy)
    #     enemy.action = :idle
    #   else
    #     enemy.action = :face_player
    #   end
    # else
    #   enemy.action = :approach
    # end
  end

  def act_movement(enemy)
    case enemy.action
    when :idle
      stop_movements(enemy)
    when :face_player
      decelerate(enemy)
      turn_towards_player(enemy)
    when :approach
      approach_range(enemy)
    end
  end


  # This tracks if the enemy is both
  # 1) in firing range, and 2) facing the player
  def in_firing_range_of_player(enemy)
    within_range?(enemy) && facing_player?(enemy)
  end

  def within_range?(enemy)
    geometry.distance(enemy, state.player_ship) <= ENEMY_RANGE
  end

  def facing_player?(enemy)
    corner_dist = 20 * ROOT_2
    ray_tests = [45, 135, 225, 315]
      .map { |x| player_center + Vect.from_degrees(corner_dist, state.player_ship.angle + x) }
      .map { |x| RayTest.new(x, enemy_facing_line(enemy)) }

    ray_tests.any? {|t| t.result == :on} ||
      ray_tests[0].facing == :towards && ray_tests[2].facing == :towards &&
        ray_tests[0].result != ray_tests[2].result ||
      ray_tests[1].facing == :towards && ray_tests[3].facing == :towards &&
        ray_tests[1].result != ray_tests[3].result
  end

  def enemy_facing_line(enemy)
    enemy_center = Vect.new(enemy.x + 18, enemy.y + 18)
    enemy_sight = enemy_center + Vect.from_degrees(1000, enemy.angle)
    {x: enemy_center.x, y: enemy_center.y, x2: enemy_sight.x, y2: enemy_sight.y}
  end

  def player_center
    ps = state.player_ship
    Vect.new(ps.x + 20, ps.y + 20)
  end

  # Fire a bullet, but only if the firing cooldown is <= 0
  def fire_if_ready(enemy)
    if enemy.fire_cooldown_current <= 0
      create_entities(state) do |b|
        vect = Vect.new(enemy.x + 15, enemy.y + 15) +
               Vect.from_degrees(20, enemy.angle)
        b.enemy_bullet(
          x: vect.x,
          y: vect.y,
          vel_x: enemy.vel_x,
          vel_y: enemy.vel_y,
          angle: enemy.angle
        )
      end
      Audio.play(args, :enemy_bullet)
      enemy.fire_cooldown_current = enemy.fire_cooldown_max
    end
  end

  def update_timers(enemy)
    if enemy.fire_cooldown_current > 0
      enemy.fire_cooldown_current -= DELTA_TIME
    end
  end

  def approach_range(enemy)
    x_target = state.player_ship.x
    y_target = state.player_ship.y
    x_vel = enemy.vel_x + 4
    y_vel = enemy.vel_y

    target_vect = Vect.new(x_target - enemy.x, y_target - enemy.y)
    target_vel_vect = Vect.from_degrees(enemy.max_vel, target_vect.angle)
    theta_target = target_vel_vect.angle

    possible_x_vel_target = target_vel_vect.x
    possible_y_vel_target = target_vel_vect.y

    x_a = possible_x_vel_target - x_vel
    y_a = possible_y_vel_target - y_vel

    acc_vect = Vect.new(x_a, y_a)

    rel_angle = acc_vect.angle_relative_to(enemy.angle)
    within_reasonable_angle = case target_vect.magnitude
    when (0..120)
      (0..1).include?(rel_angle) || (359..360).include?(rel_angle)
    when (120..240)
      (0..7).include?(rel_angle) || (353..360).include?(rel_angle)
    when (240..400)
      (0..15).include?(rel_angle) || (345..360).include?(rel_angle)
    else
      (0..20).include?(rel_angle) || (340..360).include?(rel_angle)
    end

    if within_reasonable_angle
      vel_vect = Vect.new(enemy.vel_x, enemy.vel_y)
      enemy_ship_vect = Vect.from_degrees(enemy.thrust_acc, enemy.angle)
      if vel_vect.magnitude <= enemy.max_vel ||
          (vel_vect - enemy_ship_vect).magnitude < vel_vect.magnitude
        enemy.state = :thrust unless vel_vect.magnitude > enemy.max_vel
      else
        enemy.state = :brake
      end
    else
      if rel_angle <= 180
        if rel_angle < enemy.turn_acc
          enemy.vel_angle = rel_angle
        else
          enemy.vel_angle = enemy.turn_acc
        end
      else
        if 360 - rel_angle < enemy.turn_acc
          enemy.vel_angle = rel_angle - 360
        else
          enemy.vel_angle = -enemy.turn_acc
        end
      end
    end
  end

  def decelerate(enemy)
    if enemy.vel_x && enemy.vel_y == 0
      enemy.state = :idle
    else
      enemy.state = :brake
    end
  end

  def turn_towards_player(enemy)
    ray_test = RayTest.new(player_center, enemy_facing_line(enemy))
    if ray_test.result == :on
      enemy.vel_angle = 0
    elsif ray_test.result == :clockwise
      enemy.vel_angle = -5
    else
      enemy.vel_angle = 5
    end
  end

end
