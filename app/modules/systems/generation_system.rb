class GenerationSystem < GameSystem
  include GameEntities

  def run
    state.planets.each do |planet|
      planet.generation_timer += DELTA_TIME
      if planet.generation_timer >= planet.generation_interval
        planet.generation_timer = 0
        generate_enemy(planet)
      end
    end
  end

  private

  def generate_enemy(planet)
    create_entities(state) do |b|
      b.enemy(x: planet.x, y: planet.y)
    end
  end
end
