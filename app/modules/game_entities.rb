module GameEntities
  def create_entities(state, &block)
    geb = GameEntityBuilder.new(state)
    block.call(geb)
  end

  def delete_entity(state, entity)
    return if entity == :none || entity.nil?
    state.game_entities.reject! {|e| e == entity }
    case entity.entity_type
    when :player_ship
      state.player_ship = nil
    when :planet
      state.planets.reject! {|e| e == entity}
    when :bullet
      state.bullets.reject! {|e| e == entity}
    when :enemy
      state.enemies.reject! {|e| e == entity}
    when :enemy_bullet
      state.enemy_bullets.reject! {|e| e == entity}
    when :health_bar_item
      state.healthbar.reject! {|e| e == entity}
    when :warp
      state.warp = :none
    end
  end

  def delete_all(state, entities)
    state.game_entities.reject! {|e| entities.include? e }
  end
end
