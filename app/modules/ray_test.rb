class RayTest
  attr_reader :result, :facing, :angle

  def initialize(point, line)
    point_relative_to_vect(point, line)
  end

  def fuzzy_result(offset)
    if (-offset..offset).include? (angle) ||
        (360-offset...360).include?(angle) ||
        (-360..offset-360).include?(angle)
      :on
    else
      result
    end
  end

  private

  # Considers the direction of the line, and the line's origin (x,y)
  # Comparison starts with x,y of the line as origin, and rotation occurs around that point
  # Returns
  # :clockwise if point is clockwise from vect's endpoint, or directly behind it (180deg)
  # :counter if point is counter-clockwise from vect's endpoint
  # :on if they are the same
  def point_relative_to_vect(point, line)
    point_vect = Vect.new(point.x - line.x, point.y - line.y)
    line_vect = Vect.new(line.x2 - line.x, line.y2 - line.y)

    line_angle = line_vect.angle
    point_angle = point_vect.angle

    # Range will be -359 to 359
    # -359 to -181 is CW. -180 to -1 is CCW. 0 is ON. 1 to 180 is CW. 181 to 359 is CCW
    angle_diff = line_angle - point_angle
    @angle = angle_diff
    @result = if angle_diff == 0
      :on
    elsif (-360...-180).include?(angle_diff) || (0..180).include?(angle_diff)
      :clockwise
    else
      :counter
    end

    @facing = if angle_diff == 0 ||
      (-90..90).include?(angle_diff) ||
      (270...360).include?(angle_diff) ||
      (-360..-270).include?(angle_diff)
      :towards
    else
      :away
    end
  end
end
