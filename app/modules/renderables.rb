class Renderables
  class << self
    def player_ship(r)
      renderables = [
        {
          type: :sprite,
          obj: {
            x: r.x,
            y: r.y,
            w: 40,
            h: 40,
            angle: r.angle - 90,
            path: "sprites/player/ship2.png"
          }
        }
      ]
      renderables << player_ship_thrust(r) if r.state == :thrust
      renderables
    end

    def player_ship_thrust(r)
      {
        type: :sprite,
        obj: {
          x: r.x,
          y: r.y - 10,
          w: 40,
          h: 10,
          angle_anchor_x: 0.5,
          angle_anchor_y: 3.0,
          angle: r.angle - 90,
          path: "sprites/player/thrust2.png"
        }
      }
    end

    def enemy(r)
      [
        {
          type: :sprite,
          obj: {
            x: r.x,
            y: r.y,
            w: 36,
            h: 36,
            angle: r.angle + 90,
            path: "sprites/enemy/enemy_red.png"
          }
        }
      ]
    end

    def planet(r)
      [{
        type: :sprite,
        obj: {
          x: r.x,
          y: r.y,
          w: 80,
          h: 80,
          path: "sprites/planet/planet.png"
        }
      }]
    end

    def label(r)
      [{
        type: :label,
        obj: {
          x: r.x,
          y: r.y,
          text: r.text,
          r: 255,
          g: 255,
          b: 255
        }
      }]
    end

    def bullet(r)
      [{
        type: :solid,
        obj: {
          x: r.x,
          y: r.y,
          w: 5,
          h: 5,
          angle: r.angle,
          r: r.r || 255,
          g: r.g || 0,
          b: r.b || 0
        }
      }]
    end

    def healthy_heart(r)
      renderables = []
      renderables << {
        type: :sprite,
        obj: {
          x: r.x,
          y: r.y,
          w: 10,
          h: 10,
          path: "sprites/ui/heart.png"
        }
      } if r.healthy
      renderables
    end

    def warp(r)
      [{
        type: :sprite,
        obj: {
          x: r.x,
          y: r.y,
          w: 60,
          h: 60,
          path: "sprites/planet/portal.png"
        }
      }]
    end
  end
end
