class Game
  attr_gtk
  include Rendering
  include GameEntities

  SYSTEMS = [
    UserInputSystem.new,
    PhysicsSystem.new,
    DiagnosticsSystem.new,
    BoundaryWarpSystem.new,
    BulletSystem.new,
    DamageSystem.new,
    AiSystem.new,
    UiSystem.new,
    GenerationSystem.new,
    SceneSystem.new
  ]

  def initialize(args)
    initialize_state(args.state)
    SYSTEMS.each {|s| s.init(args)}
    LevelGenerator.new(args).generate_title
    Audio.loop(args, :bgm)
  end

  def initialize_state(state)
    state.inspection.pixel_grid ||= false
    state.planets ||= []
    state.bullets ||= []
    state.enemy_bullets ||= []
    state.enemies ||= []
    state.game_entities ||= []
    state.healthbar ||= []
    state.labels ||= {}
    state.score ||= 0
    state.score_meta = {
      max_from_enemies: 1000,
      current_from_enemies: 0
    }
    state.player_data ||= {}
    state.ready_to_play ||= :no
    state.debug_paused ||= :no
    state.volume ||= 1.0
  end

  def tick
    render

    if inputs.keyboard.key_down.p
      if state.debug_paused == :no
        state.debug_paused = :yes
      else
        state.debug_paused = :no
      end
    end

    run_systems unless state.debug_paused == :yes
  end

  def run_systems
    SYSTEMS.each { |sys| sys.run_system(args) }
  end

  def render
    labels = []
    lines = []
    solids = []
    sprites = []

    state.game_entities.each do |entity|
      renderables = renderables_for(entity)
      renderables.each do |renderable|
        case renderable[:type]
        when :label
          labels << renderable[:obj]
        when :line
          lines << renderable[:obj]
        when :solid
          solids << renderable[:obj]
        when :sprite
          sprites << renderable[:obj]
        end
      end
    end

    outputs.background_color = [0, 0, 0]
    outputs.labels << labels
    outputs.lines << lines
    outputs.solids << solids
    outputs.sprites << sprites
  end
end
