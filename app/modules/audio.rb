class Audio
  class << self
    def play(args, type, **kwargs)
      increment_counter
      sound_id = "#{type}_#{@@sound_counter}"
      args.audio[sound_id] = defaults.merge(send(type)).merge(kwargs).tap do |config|
        config.gain *= args.state.volume
      end
      sound_id
    end

    def loop(args, label, **kwargs)
      args.audio[label] = defaults.merge(send(label)).merge(kwargs).tap do |config|
        config.gain *= args.state.volume
      end
      remember_label(label)
      label
    end

    def inc_volume(args)
      @@old_volume = args.state.volume
      args.state.volume += 0.1 unless args.state.volume == 2
      set_looped_volume(args)
    end

    def dec_volume(args)
      @@old_volume = args.state.volume
      args.state.volume -= 0.1 unless args.state.volume == 0
      set_looped_volume(args)
    end

    def increment_counter
      @@sound_counter ||= 0
      @@sound_counter += 1
      @@sound_counter = 0 if @@sound_counter > 2_000_000_000
    end

    def remember_label(label)
      @@labels ||= []
      @@labels << label
    end

    def set_looped_volume(args)
      @@labels.each do |label|
        audio = args.audio[label]
        base_gain = audio.gain / @@old_volume
        audio.gain = base_gain * args.state.volume
      end
    end

    def pause(args, label)

    end

    private

    def defaults
      {
        x: 0, y: 0, z: 0,
        paused: false,
        looping: false
      }
    end

    def bullet
      {
        input: "sounds/astrolet-sfx-ship_bullet_02.ogg",
        gain: 0.45,
        pitch: (7 + Random.rand(7)) / 10,
      }
    end

    def enemy_bullet
      {
        input: "sounds/astrolet-sfx-ship_bullet_02.ogg",
        gain: 0.4,
        pitch: (20 + Random.rand(15)) / 100,
      }
    end

    def enemy_death
      {
        input: "sounds/astrolet-sfx-enemy_explosion_01.ogg",
        gain: 0.8,
        pitch: (7 + Random.rand(7)) / 10,
      }
    end

    def enemy_ship_damage
      {
        input: "sounds/astrolet-sfx-ship_damage_01.ogg",
        gain: 0.4,
        pitch: (7 + Random.rand(7)) / 10,
      }
    end

    def planet_explosion
      {
        input: "sounds/astrolet-sfx-planet_explosion_01.ogg",
        gain: 0.4,
        pitch: (8 + Random.rand(5)) / 10,
      }
    end

    def player_death
      {
        input: "sounds/astrolet-sfx-ship_explosion_01.ogg",
        gain: 0.3,
        pitch: (7 + Random.rand(7)) / 10,
      }
    end

    def ship_damage
      {
        input: "sounds/astrolet-sfx-ship_damage_01.ogg",
        gain: 0.3,
        pitch: (7 + Random.rand(7)) / 10,
      }
    end

    def bgm
      {
        input: "sounds/game_jams-ryan-bgm-master_01.ogg",
        gain: 0.1,
        pitch: 1.0,
        looping: true
      }
    end
  end
end
