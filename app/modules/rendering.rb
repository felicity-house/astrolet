module Rendering
  def renderables_for(renderable)
    Renderables.send(renderable.renderable_name, renderable)
  end
end
