class GameEntityBuilder
  def initialize(state)
    @state = state
  end

  def player_ship(**kwargs)
    @state.player_ship = @state.new_entity(:player_ship, {
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 40,
      h: 40,
      vel_x: 0,
      vel_y: 0,
      angle: 90,
      health: kwargs.health || 10,
      is_dead: false,
      state: :idle, # :idle, :thrust, :brake
      thrust_acc: 0.1,
      brake_acc: -0.09,
      vel_angle: 0,
      renderable_name: :player_ship
    })
    add_game_entity(@state.player_ship)
  end

  def enemy(**kwargs)
    enemy = @state.new_entity(:enemy, {
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 36,
      h: 36,
      health: 2,
      vel_x: 0,
      vel_y: 0,
      angle: 90,
      state: :idle, # :idle, :thrust, :brake
      action: :idle, # :idle, :turn_to_dest, :acc_to_dest, :dec_to_dest, :face_player
      max_vel: 6,
      thrust_acc: 0.1,
      brake_acc: -0.09,
      vel_angle: 0,
      turn_acc: 3,
      renderable_name: :enemy,
      fire_cooldown_max: 1000,
      fire_cooldown_current: 0
    })
    @state.enemies << enemy
    add_game_entity(enemy)
  end

  def planet(**kwargs)
    planet = @state.new_entity(:planet, {
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 80,
      h: 80,
      health: 12,
      renderable_name: :planet,
      generation_interval: kwargs[:generation_interval] || 8000,
      generation_timer: kwargs[:generation_timer] || 0,
      contains_warp: false
    })
    @state.planets << planet
    add_game_entity(planet)
  end

  def diagnostic(key, **kwargs)
    @state.diagnostics ||= {}
    index = @state.diagnostics.count
    @state.diagnostics[key] = label(x: 900, y: 700 - index * 20, **kwargs)
  end

  def label(**kwargs)
    label = {
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      text: kwargs[:text] || "no text",
      renderable_name: :label
    }
    @state.labels[kwargs.id] = label if kwargs.id
    add_game_entity(label)
  end

  def bullet(**kwargs)
    velocity = Vect.new(kwargs[:vel_x], kwargs[:vel_y]) + Vect.from_degrees(12, kwargs[:angle])
    bullet = @state.new_entity(:bullet, {
      renderable_name: :bullet,
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 5,
      h: 5,
      r: 255,
      b: 255,
      g: 255,
      vel_x: velocity.x,
      vel_y: velocity.y,
      angle: kwargs[:angle],
      time_remaining: 1300
    })
    @state.bullets << bullet
    add_game_entity(bullet)
  end

  def enemy_bullet(**kwargs)
    velocity = Vect.new(kwargs[:vel_x], kwargs[:vel_y]) + Vect.from_degrees(12, kwargs[:angle])
    bullet = @state.new_entity(:enemy_bullet, {
      renderable_name: :bullet,
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 5,
      h: 5,
      vel_x: velocity.x,
      vel_y: velocity.y,
      angle: kwargs[:angle],
      time_remaining: 1300
    })
    @state.enemy_bullets << bullet
    add_game_entity(bullet)
  end

  def health_bar_item(**kwargs)
    item = @state.new_entity(:health_bar_item, {
      renderable_name: :healthy_heart,
      x: kwargs[:x] || 0,
      y: kwargs[:y] || 0,
      w: 10,
      h: 10,
      healthy: false
    })
    @state.healthbar << item
    add_game_entity(item)
  end

  def warp(**kwargs)
    warp = @state.new_entity(:warp, {
      renderable_name: :warp,
      x: kwargs.x || 0,
      y: kwargs.y || 0,
      w: 60,
      h: 60
    })
    @state.warp = warp
    add_game_entity(warp)
  end

  private

  def add_game_entity(entity)
    @state.game_entities << entity
    entity
  end
end
