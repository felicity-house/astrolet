class Vect
  attr_accessor :x, :y

  def initialize(x, y)
    @x = x
    @y = y
  end

  def self.from_degrees(magnitude, angle)
    x = magnitude * Math.cos(angle * PI * OVER_180)
    y = magnitude * Math.sin(angle * PI * OVER_180)
    self.new(x, y)
  end

  def add(vect)
    Vect.new(x + vect.x, y + vect.y)
  end

  def +(vect)
    add(vect)
  end

  def subtract(vect)
    Vect.new(x - vect.x, y - vect.y)
  end

  def -(vect)
    subtract(vect)
  end

  def add_magnitude(added_magnitude)
    Vect.from_degrees(magnitude + added_magnitude, angle)
  end

  def angle
    if x.zero?
      return 0 if y.zero?
      return 90 if y > 0
      return 270
    end
    result = Math.atan(y/x) * OVER_PI * 180
    if x < 0
      result += 180
    elsif y < 0
      result += 360
    end
    result
  end

  def magnitude
    (x**2 + y**2)**0.5
  end

  def angle_relative_to(vect)
    rel_angle = angle - (vect.is_a?(Vect) ? vect.angle : vect)
    rel_angle += 360 if rel_angle < 0
    rel_angle
  end

  def normal
    mag = magnitude
    Vect.new(x/mag, y/mag)
  end

  def to_s
    "Vect(#{x.round(2)}, #{y.round(2)})"
  end
end
